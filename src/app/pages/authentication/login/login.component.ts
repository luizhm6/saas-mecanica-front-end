import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { AuthService } from "src/app/auth/auth.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class LoginComponent implements OnInit {
  constructor(private service: AuthService) {}

  ngOnInit() {}

  login() {
    this.service.login("luizhm6@gmail.com", "123456").subscribe((res) => {
      console.log(res);
    });
  }
}
