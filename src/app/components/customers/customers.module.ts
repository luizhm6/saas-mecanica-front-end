import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomersListComponent } from './customers-list/customers-list.component';
import { CustomersCreateComponent } from './customers-create/customers-create.component';
import { CustomerRoutingModule } from './customers-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { CustomersService } from './customers.service';

@NgModule({
  declarations: [CustomersListComponent, CustomersCreateComponent],
  imports: [
    CommonModule,
    CustomerRoutingModule,
    SharedModule,
    ReactiveFormsModule,
  ],
  providers:[ CustomersService]
})
export class CustomersModule {}
