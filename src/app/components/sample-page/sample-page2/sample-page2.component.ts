import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { ItemOrdemServico } from 'src/app/shared/models/OS/itemOrdemServico';

@Component({
  selector: 'app-sample-page2',
  templateUrl: './sample-page2.component.html',
  styleUrls: ['./sample-page2.component.scss'],
})
export class SamplePage2Component implements OnInit {
  ordemDeServicoForm: FormGroup = this.createForm();
  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
  }

  createForm() {
    return this.fb.group({
      nome: null,
      telefone: null,
      veiculo: null,
      itens: this.fb.array([]),
    });
  }
  get itens() {
    return this.ordemDeServicoForm.controls['itens'] as FormArray;
  }
  addItem() {
    let novoItem = new FormGroup(new ItemOrdemServico());
    this.itens.push(novoItem);
    console.log(this.ordemDeServicoForm.value)
  }
}
