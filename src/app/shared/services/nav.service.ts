import { Injectable, HostListener } from '@angular/core';
import { BehaviorSubject, Observable, Subscriber } from 'rxjs';

// Menu
export interface Menu {
	path?: string;
	title?: string;
	icon?: string;
	type?: string;
	badgeType?: string;
	badgeValue?: string;
	active?: boolean;
	bookmark?: boolean;
	children?: Menu[];
}

@Injectable({
	providedIn: 'root'
})

export class NavService {

	public screenWidth: any
	public collapseSidebar: boolean = false
	public fullScreen = false;

	constructor() {
		this.onResize();
		if (this.screenWidth < 991) {
			this.collapseSidebar = true
		}
	}

	// Windows width
	@HostListener('window:resize', ['$event'])
	onResize(event?:any) {
		this.screenWidth = window.innerWidth;
	}

	MENUITEMS: Menu[] = [
		{
			title: "ORDENS DE SERVIÇO",
			icon: "home",
			type: "sub",
			active: true,
			children: [
			  { path: "/sample-page/sample-page1", title: "Listagem de ordens de serviço", type: "link" },
			  { path: "/sample-page/sample-page2", title: "Criar ordem de serviço", type: "link" },
			],
		 },{
			title: "Clientes",
			icon: "home",
			type: "sub",
			active: true,
			children: [
			  { path: "/customers", title: "Listagem de clientes", type: "link" },
			  { path: "/customers/form", title: "Cliente", type: "link" },
			],
		 },
		 { path: "/inventory", icon: "home", title: "Estoque", type: "link" },
		 { path: "/billing", icon: "home", title: "Entradas e saidas", type: "link" },
		 { path: "/sample-page3", icon: "home", title: "sample-page-3", type: "link" },
	]
	// Array
	items = new BehaviorSubject<Menu[]>(this.MENUITEMS);


}
