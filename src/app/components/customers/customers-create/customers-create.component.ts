import { Component } from "@angular/core";
import { CustomersService } from "../customers.service";
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { Customer } from "src/app/shared/models/customer.interface";

@Component({
  selector: "app-customers-create",
  templateUrl: "./customers-create.component.html",
  styleUrls: ["./customers-create.component.scss"],
})
export class CustomersCreateComponent {
  customerForm: FormGroup;
  customer: Customer;
  constructor(
    private service: CustomersService,
    private formBuilder: FormBuilder
  ) {
    this.customerForm = this.formBuilder.group({
      name: ["", Validators.required],
      email: ["", [Validators.required, Validators.email]],
      phone: ["", Validators.required],
      cpf: ["", Validators.required],
      address: this.formBuilder.group({
        street: ["", Validators.required],
        number: ["", Validators.required],
        neighborhood: ["", Validators.required],
        city: ["", Validators.required],
        state: ["", Validators.required],
        zipCode: ["", Validators.required],
      }),
      vehicles: this.formBuilder.array([]),
    });
  }

  public addVehicles() {
    const vheicle = this.formBuilder.group({
      model: ["", Validators.required],
      year: ["", Validators.required],
      licensePlate: ["", Validators.required],
      color: ["", Validators.required],
      services: this.formBuilder.array([]),
    });
    return (this.customerForm.get("vehicles") as FormArray).push(vheicle);
  }

  get vehicles() {
    return (this.customerForm.get("vehicles") as FormArray).controls as FormGroup[];
  }
  get address() {
    return this.customerForm.get("address") as FormGroup;
  }

  get formControls() {
    return this.customerForm.controls;
  }

  insert() {
    if (this.customerForm.invalid) {
      return;
    }

    this.service.insert(this.customerForm.value);
  }
}
