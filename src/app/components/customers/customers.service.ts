import { Injectable } from "@angular/core";
import { AngularFireDatabase } from "@angular/fire/compat/database";
import { Customer } from "src/app/shared/models/customer.interface";

@Injectable({
  providedIn: "root",
})
export class CustomersService {
  constructor(private fb: AngularFireDatabase) {}

  insert(customer: Customer) {
    this.fb.list("/customers").push(customer);
  }
}
