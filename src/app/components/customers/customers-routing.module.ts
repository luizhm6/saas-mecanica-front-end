import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomersListComponent } from './customers-list/customers-list.component';
import { CustomersCreateComponent } from './customers-create/customers-create.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: CustomersListComponent,
        data: {
          title: "Listagem de clientes",
          breadcrumb: "clientes"
        }
      },
      {
        path: 'form',
        component: CustomersCreateComponent,
        data: {
          title: "Cadastrar cliente",
          breadcrumb: "cliente/criar"
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRoutingModule { }
