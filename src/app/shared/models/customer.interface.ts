export interface Part {
  name: string;
  value: string;
  quantity: string;
  brand: string;
}

export interface Service {
  name: string;
  value: string;
  date: string;
  status: string;
  company: string;
  parts?: Part;
}

export interface Address {
  street: string;
  number: string;
  neighborhood: string;
  city: string;
  state: string;
  zipCode: string;
}

export interface Vehicle {
  model: string;
  year: string;
  licensePlate: string;
  color: string;
  services?: Service;
}

export class Customer {
  name: string;
  email: string;
  phone: string;
  cpf: string;
  address?: Address;
  vehicles?: Vehicle[];

  constructor(
    name: string,
    email: string,
    phone: string,
    cpf: string,
    address: Address,
    vehicles?: Vehicle[]
  ) {
    this.name = name;
    this.email = email;
    this.phone = phone;
    this.cpf = cpf;
    this.vehicles = vehicles;
    this.address = address;
  }
}
