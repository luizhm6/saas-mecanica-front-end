import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseUrl = environment.baseUrl
  constructor(private http: HttpClient) { }

  login(user, pass): Observable<any>{
    let headers: HttpHeaders = new HttpHeaders().set('Access-Control-Allow-Origin', '*').append('Referrer-Policy', 'no-referrer')
    return this.http.post(`${this.baseUrl}/users/authenticate`, {email: user, password: pass}, {headers: headers})
  }

}
