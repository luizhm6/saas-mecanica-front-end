// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  baseUrl: 'localhost:3000',
  firebaseConfig: {
    apiKey: "AIzaSyCF_oN56IJYEDWXBBjrrYgzuPxOrvceR6o",
    authDomain: "saas-mecanica.firebaseapp.com",
    projectId: "saas-mecanica",
    storageBucket: "saas-mecanica.appspot.com",
    messagingSenderId: "784125466470",
    appId: "1:784125466470:web:6035698a7b4d4674185aa7",
    measurementId: "G-QSWQ8CYRLP",
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
