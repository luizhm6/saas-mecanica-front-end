import { FormControl } from "@angular/forms";

export class ItemOrdemServico {
  qtd: FormControl;
  descricao: FormControl;
  valorUnitario: FormControl;
  valorTotal: FormControl;
  constructor(
    qtd: number = 0,
    descricao: string = '',
    valorUnitario: number = 0,
    valorTotal: number = 0
  ) {
    this.qtd = new FormControl(qtd);
    this.descricao = new FormControl(descricao);
    this.valorUnitario = new FormControl(valorUnitario);
    this.valorTotal = new FormControl(valorTotal);
  }
}
