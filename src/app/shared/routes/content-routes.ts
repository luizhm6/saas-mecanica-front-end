import { Routes } from '@angular/router';

export const content: Routes = [
  {
    path: "sample-page",
    loadChildren: () => import("../../components/sample-page/sample-page.module").then((m) => m.SamplePageModule),
  },
  {
    path: "sample-page3",
    loadChildren: () => import("../../components/sample-page3/sample-page3.module").then((m) => m.SamplePage3Module),
  },
  {
    path: "customers",
    loadChildren: () => import("../../components/customers/customers.module").then((c) => c.CustomersModule),
  },
  {
    path: "inventory",
    loadChildren: () => import("../../components/inventory/inventory.module").then((i) => i.InventoryModule),
  },
  {
    path: "billing",
    loadChildren: () => import("../../components/billing/billing.module").then((b) => b.BillingModule),
  },
];
